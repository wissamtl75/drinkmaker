package com.Drink;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

public class AppTestDrink {
	CommandeDrink	commandeDrink =new CommandeDrink();

    @Test
    public void testCommandeDrink() {
    	assertEquals(commandeDrink.prepareDrink("Coffee","hot", 0, 0.1), "solde insufisant, il manque : 0.5");
        assertEquals(commandeDrink.prepareDrink("Coffee","", 0, 0.7), "C::");;
        assertEquals(commandeDrink.prepareDrink("Coffee","hot", 2, 0.7), "Ch:2:0");;
        
        assertEquals(commandeDrink.prepareDrink("Orange","", 0, 0.6), "O::");
        assertEquals(commandeDrink.prepareDrink("Orange","", 5, 0.6), "O:5:0");
        assertEquals(commandeDrink.prepareDrink("Orange","", 5, 0.6), "O:5:0");
        assertEquals(commandeDrink.prepareDrink("Orange","", 1, 0.3), "solde insufisant, il manque : 0.3");
        
        
        assertEquals(commandeDrink.prepareDrink("Tea","", 0, 0.6), "T::");
        assertEquals(commandeDrink.prepareDrink("Tea","hot", 0, 0.6), "Th::");
        assertEquals(commandeDrink.prepareDrink("Tea","", 2, 0.6), "T:2:0");
        assertEquals(commandeDrink.prepareDrink("Tea","hot", 2, 0.6), "Th:2:0");
        assertEquals(commandeDrink.prepareDrink("Tea","hot", 1, 0.3), "solde insufisant, il manque : 0.1");
        
        assertEquals(commandeDrink.prepareDrink("Chocolate","hot", 0, 0.6), "Hh::");
        assertEquals(commandeDrink.prepareDrink("Chocolate","", 2, 0.6), "H:2:0");
        assertEquals(commandeDrink.prepareDrink("Chocolate","hot", 2, 0.6), "Hh:2:0");
        assertEquals(commandeDrink.prepareDrink("Chocolate","hot", 0, 0.4), "solde insufisant, il manque : 0.1");
    }

}
