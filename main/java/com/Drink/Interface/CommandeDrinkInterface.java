package com.Drink.Interface;

import com.Drink.CommandeDrink;

public interface CommandeDrinkInterface {
	
	String prepareDrink(String drink,  String temperatureCode, int Sucre , double montant);

}
