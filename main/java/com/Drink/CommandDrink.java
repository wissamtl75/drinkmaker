package com.Drink;
import java.math.BigDecimal;
import com.Drink.Interface.CommandeDrinkInterface;


public class CommandDrink implements CommandeDrinkInterface {
	

	public String prepareDrink(String drink, String temperatureCode, int sucre , double monnaieClient ) {
		
  	  String  retourMachine = "" ;
  	  String  stick = sucre > 0 ? "0" : "";
  	  String  quantitesucre = sucre > 0 ? sucre+"" : "";
  	  double montantDrink=0;
  	  BigDecimal resteApayer=new BigDecimal(0);
  	  
  	  for(DrinkCatalog d : DrinkCatalog.values()) {
  		  
  	         if(d.getNameDrink().equals(drink)) {
  	        	 
  	        	 montantDrink=d.getPrix();
  	        	 if (temperatureCode.equals("hot")) {
  	        		 
  	        		retourMachine = retourMachine+d.getCode()+d.getTemperatureCode();
  	        	 }else {
  	        		 
  	        		retourMachine=retourMachine+d.getCode(); 

  	        	 }
  	        	
  	         }
  	      }
  	  
  	  if(montantDrink > monnaieClient) {
  		  
  		resteApayer=BigDecimal.valueOf(montantDrink).subtract(BigDecimal.valueOf(monnaieClient));
  		retourMachine="solde insufisant, il manque : "+ resteApayer;
  		
  	  }
  	  else {
  		retourMachine=retourMachine+":"+quantitesucre+":"+stick;
  		
  	  }
  	  return retourMachine ;
  	  

	}

}
