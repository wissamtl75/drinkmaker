package com.Drink;

public enum DrinkCatalog {
Coffee("Coffee","C","h",0.6) ,
TEA("Tea","T","h",0.4), 
CHOCOLATE("Chocolate","H","h",0.5),
ORANGE("Orange","O","",0.6) ,;

private String nameDrink ;
private String code ;
private String temperatureCode;
private double prix ;
	

DrinkCatalog(String nameDrink , String Code , String temperatureCode ,double prix) {
	this.nameDrink=nameDrink;
	this.code=Code;
	this.temperatureCode=temperatureCode;
	this.prix=prix;
}

public String getNameDrink() {
	return nameDrink;
}


public void setNameDrink(String nameDrink) {
	this.nameDrink = nameDrink;
}


public String getCode() {
	return code;
}


public String getTemperatureCode() {
	return temperatureCode;
}

public void setTemperatureCode(String temperatureCode) {
	this.temperatureCode = temperatureCode;
}

public void setCode(String code) {
	this.code = code;
}

public double getPrix() {
	return prix;
}

public void setPrix(double prix) {
	this.prix = prix;
}


}
