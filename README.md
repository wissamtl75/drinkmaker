Bienvenue dans mon application de gestion de boisson:

- Nous avons dans ce projet la classe DrinkCatalog dans laquelle sont recensés l'ensemble des boissons proposés par notre machine
et la ou d'autres boissons peuvent etre rajoutés/retirés du catalogue.

- les paramètres qui composent la boisson sont : - Le nom de la boisson. 
					       - La temperature de la boisson.
					       - Le nombre de sucres sélectionnés. 
					       - La monnaie soumise par la client.

- La seconde class CommandeDrink vient ici traités la demande du client (fonction : prepareDrink(); ) selon les differents besoin et renvoie le code attendus au fabricant de boissons.

- l'interface CommandeDrinkInterface est implémentés à CommandeDrink en cas de futur besoin.

- notre class de test : AppTestDrink , avec les différents test unitaires qui viennent tester nos differents use Case :
(code attendu, temperature de la boisson , quantité de sucres , présence ou non du stick , verification de la monnaie soumise par le client).




